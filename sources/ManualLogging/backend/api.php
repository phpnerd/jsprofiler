<?php

$DB = mysqli_connect("localhost", "perfanalysis", "password", "perfanalysis") or die("Failed to connect to database!");
$stmt = null;

function insertCall($call, $parent, $profileId) {
    global $stmt;
    $stmt->bind_param("isii", $profileId, $call->functionName, $call->executionTime, $parent);
    if(!$stmt->execute())
        die("Failed to store function call: ".$stmt->error);
    $subParent = $stmt->insert_id;
    foreach($call->subFunctions as $subCall)
        insertCall($subCall, $subParent, $profileId);
}

function storeProfile() {
    global $DB, $stmt;
    $profile = json_decode($_REQUEST['profile']);

    $stmt = $DB->prepare("INSERT INTO `analyses` (`executiontime`, `idletime`) VALUES (?,?)");
    $stmt->bind_param("ii", $profile->executionTime, $profile->idleTime);
    if (!$stmt->execute())
        die("Failed to store profile: " . $stmt->error);
    $profileId = $stmt->insert_id;
    $stmt->close();

    $stmt = $DB->prepare("INSERT INTO `funcalls` (`run`, `name`, `executiontime`, `parent`) VALUES (?,?,?,?)");
    foreach ($profile->subFunctions as $child)
        insertCall($child, null, $profileId);
    $stmt->close();

    $DB->close();

    echo "OK";
}

function loadFunCalls($profileId, $parentId) {
    global $DB, $stmt;
    $result = array();
    if (!is_null($parentId)) {
        $stmt = $DB->prepare("SELECT F.`id`, F.`run`, F.`name`, F.`executiontime`, F.`parent`, (SELECT COUNT(*) FROM `funcalls` FC WHERE FC.`parent` = F.`id`) as `children` FROM `funcalls` F WHERE `run` = ? AND `parent` = ?");
        $stmt->bind_param("ii", $profileId, $parentId);
    } else {
        $stmt = $DB->prepare("SELECT F.`id`, F.`run`, F.`name`, F.`executiontime`, F.`parent`, (SELECT COUNT(*) FROM `funcalls` FC WHERE FC.`parent` = F.`id`) as `children` FROM `funcalls` F WHERE `run` = ? AND `parent` IS NULL");
        $stmt->bind_param("i", $profileId);
    }
    $stmt->bind_result($id, $run, $name, $executionTime, $parent, $children);
    if(!$stmt->execute())
        die("Failed to fetch function calls: ".$stmt->error);
    $i = 0;
    while($stmt->fetch())
        $result[$i++] = array('id' => $id, 'run' => $run, 'functionName' => $name, 'executionTime' => $executionTime, 'parent' => $parent, 'children' => $children);
    $stmt->close();
    return $result;
}

function loadProfiles() {
    global $DB, $stmt;
    $result = array();
    $stmt = $DB->prepare("SELECT * FROM `analyses` ORDER BY `time` DESC");
    $stmt->bind_result($id, $time, $executionTime, $idleTime);
    if(!$stmt->execute())
        die("Failed to fetch profiles: ".$stmt->error);
    $i = 0;
    while($stmt->fetch())
        $result[$i++] = array('id' => $id, 'time' => $time, 'executionTime' => $executionTime, 'idleTime' => $idleTime);
    echo json_encode($result);
}

function loadProfile() {
    global $DB, $stmt;
    $profile = $_REQUEST['profile'];
    $stmt = $DB->prepare("SELECT * FROM `analyses` WHERE `id` = ?");
    $stmt->bind_param("i", $profile);
    $stmt->bind_result($id, $time, $executionTime, $idleTime);
    if(!$stmt->execute())
        die("Failed to fetch profile: ".$stmt->error);
    $stmt->fetch();
    $result = array('id' => $id, 'time' => $time, 'executionTime' => $executionTime, 'idleTime' => $idleTime, 'subFunctions' => array());
    $stmt->close();
    $result['subFunctions'] = loadFunCalls($result['id'], null);
    echo json_encode($result);
}

function loadProfilePart() {
    $profile = $_REQUEST['profile'];
    $parent = $_REQUEST['parent'];
    echo json_encode(loadFunCalls($profile, $parent));
}

if (isset($_REQUEST['get']) && !isset($_REQUEST['profile']))
    loadProfiles();
elseif (isset($_REQUEST['get']) && !isset($_REQUEST['parent']))
    loadProfile();
elseif (isset($_REQUEST['get']) && isset($_REQUEST['parent']))
    loadProfilePart();
elseif (isset($_REQUEST['store']))
    storeProfile();
else
    die("No method selected!");

?>