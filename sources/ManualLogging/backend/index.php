<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<title>Profiler</title>
	<style>
		li a {
			font-weight: bold;
			cursor: pointer;
		}
	</style>
	<script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
	<script type="text/javascript">
		function appendChildren(children, parentEl) {
			var list = $("<ul>");
			children.forEach(function(f) {
				list.append($("<li>")
					.append($("<a>")
						.html(f.functionName + ": ")
						.click(function() {
							var children = $(this).parent().children("ul");
							if (children.length === 0)
								fetchChildren(f.run, f.id);
							else
								children.toggle();
						}))
					.append(f.executionTime + " ms ")
					.append(" (" + f.children + " child" + (f.children === 1 ? "" : "ren") + ")")
					.attr("id", "profile_" + f.run + "_" + f.id));
			});
			parentEl.append(list);
		}

		function fetchChildren(profileId, parentId) {
			var parentEl;
			var obj = {
				"get": true,
				"profile": profileId
			};
			if (parentId !== null) {
				obj.parent = parentId;
				parentEl = $("#profile_" + profileId + "_" + parentId);
			} else
				parentEl = $("#profile_" + profileId);
			$.ajax({
				method: "POST",
				url: "api.php",
				data: obj,
				dataType: "json"
			}).done(function(resp) {
				if(resp.subFunctions)
					appendChildren(resp.subFunctions, parentEl);
				else
					appendChildren(resp, parentEl);
			});
		}

		$(function() {
			$.ajax({
				method: "POST",
				url: "api.php",
				data: {
					"get": true
				},
				dataType: "json"
			}).done(function(resp) {
				resp.forEach(function(p) {
					$("#profiles").append($("<li>")
						.append($("<a>")
							.html(p.time + ": ")
							.click(function() {
								var children = $(this).parent().children("ul");
								if (children.length === 0)
									fetchChildren(p.id, null);
								else
									children.toggle();
							}))
						.append(p.executionTime + " ms / " + p.idleTime + " ms")
						.append(" | " + (p.executionTime - p.idleTime) + " ms")
						.attr("id", "profile_" + p.id));
				});
			});
		});
	</script>
</head>
<body>
	<h3>Profiles</h3>
	<ul id="profiles"></ul>
</body>
</html>