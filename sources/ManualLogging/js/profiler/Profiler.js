var FunctionCall = function (functionName) {
  this.functionName = functionName;
  this.subFunctions = [];
  this.executionTime = new Date();
};

FunctionCall.prototype.calculateTime = function () {
  this.executionTime = new Date() - this.executionTime;
};

RootCall.prototype = new FunctionCall();
RootCall.prototype.constructor = RootCall;
function RootCall() {
  this.functionName = 'Main';
  this.subFunctions = [];
  this.idleTime = null;
  this.executionTime = new Date();
}

RootCall.prototype.calculateTime = function () {
  FunctionCall.prototype.calculateTime.call(this);
  var idleTime = this.executionTime;
  this.subFunctions.forEach(function (e) {
    idleTime -= e.executionTime;
  });
  this.idleTime = idleTime;
};

var Profiler = function () {

  /* === ATTRIBUTE DECLARATIONS === */
  var active = true,
    profile = new RootCall('Main'),
    tmp = null,
    depth = 0;

  /* === PUBLIC METHODS === */

  this.start = function (functionName) {
    if (!assertActive('start')) {
      return;
    }
    getScope().push(new FunctionCall(functionName));
    depth++;
  };

  this.stop = function () {
    if (!assertActive('stop')) {
      return;
    }
    depth--;
    tmp = getScope();
    tmp = tmp[tmp.length - 1];
    tmp.calculateTime();
  };

  this.close = function () {
    if (!assertActive('close')) {
      return;
    }
    depth = 0;
    tmp = getRootScope();
    tmp.calculateTime();
    active = false;
    return tmp;
  };

  this.toString = function () {
    return JSON.stringify(getRootScope());
  };

  this.print = function () {
    console.log('Profile:\n' + this);
  };

  this.save = function () {
    if (!assertClosed('save')) {
      return;
    }
    var xmlhttp;
    if (window.XMLHttpRequest) {
      xmlhttp = new window.XMLHttpRequest();
    } else {
      xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
    }

    xmlhttp.onreadystatechange = function () {
      if (xmlhttp.readyState == 4) {
        if (xmlhttp.status == 200) {
          if (xmlhttp.responseText === 'OK') {
            console.log('Profile stored.');
          } else {
            console.log('Error: ' + xmlhttp.responseText);
          }
        } else {
          console.log('An error occured while storing the profile.');
        }
      }
    };

    xmlhttp.open('POST', 'http://localhost/profiler/api.php', true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send('store=true&profile=' + JSON.stringify(getRootScope()));
  };

  /* === PRIVATE METHODS === */

  function assertActive(functionName) {
    if (!active) {
      console.log('Profiler is not active! Cannot execute \"' + functionName + '()\".');
      return false;
    }
    return true;
  }

  function assertClosed(functionName) {
    if (active) {
      console.log('Profiler is active! Cannot execute \"' + functionName + '()\".');
      return false;
    }
    return true;
  }

  function getScope() {
    var s = profile,
      i = 0;
    for (; i < depth; i++) {
      s = s.subFunctions[s.subFunctions.length - 1];
    }
    return s.subFunctions;
  }

  function getRootScope() {
    return profile;
  }

},

profiler = new Profiler();
