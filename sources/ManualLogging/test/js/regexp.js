var REGEX_ROUNDS = 100;

var regex = [
    /(htrs([b]+)lkjc|([a]*)([b]+)([a]+))/i,
    /((a|b)+)/i,
    /sxpl([a-zA-Z]{3-8})lp/,
    /ln?mas(_{2})/,
    /tra*khba/,
    /\s(abc|def|ghi|jkl|mno|pqr|stu|vwx|yz)\s([a-f]{1-3})/i,
    /^[\s\xa0]+|[\s\xa0]+$/g,
    /(((\w+):\/\/)([^\/:]*)(:(\d+))?)?([^#?]*)(\?([^#]*))?(#(.*))?/,
    /  /,
    /^\s*(([a]+)|([b]+)+)(\S*(\s+\S+)*)\s*$/,
    /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i
];

String.random = function(length) {
    profiler.start("random");
    var s = "";
    for(var i = 0; i < length; i++)
        s += chars[rand(chars.length)];
    profiler.stop();
    return s;
};

function runRegexp() {
    profiler.start("runRegexp");
    regex.forEach(function(rex) {
        profiler.start("anonymous function");
        for (var i = 0; i < REGEX_ROUNDS; i++)
            rex.exec(text);
        profiler.stop();
    });
    profiler.stop();
}
