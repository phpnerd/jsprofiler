var ARR_LENGTH = 3e4;
var ARR_ROUNDS = 1e4;
var arr, tmp, a;

function runArray() {
    profiler.start("runArray");
    for (var i = 0; i < ARR_ROUNDS; i++) {
        arr = [];
        arr.length = ARR_LENGTH;
    }

    for (var i = 0; i < ARR_ROUNDS; i++)
        arr = new Array(ARR_LENGTH);

    arr = [];
    for (var i = 0; i < ARR_ROUNDS; i++)
        arr.unshift(ARR_LENGTH);

    arr = [];
    for (var i = 0; i < ARR_ROUNDS; i++)
        arr.splice(0, 0, ARR_LENGTH);

    a = arr.slice();
    for (var i = 0; i < ARR_ROUNDS; i++)
        tmp = a.shift();

    a = arr.slice();
    for (var i = 0; i < ARR_ROUNDS; i++)
        arr.splice(0, 1);

    arr = [];
    for (var i = 0; i < ARR_ROUNDS; i++)
        arr.push(i);

    a = arr.slice();
    for (var i = 0; i < ARR_ROUNDS; i++)
        tmp = a.pop();
    profiler.stop();
}