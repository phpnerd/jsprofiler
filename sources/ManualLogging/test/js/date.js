var DATE_ROUNDS = 3.5e4;
var DATE_STEP = 652918732;

Date.prototype.formatDate = function (input, time) {
    profiler.start("formatDate");
    var methods = ["Y", "m", "M", "d", "D", "H", "i", "s"];
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

    function Y() {
        return self.getFullYear();
    }

    function m() {
        var m = self.getMonth() + 1;
        return m < 10 ? "0" + m : m;
    }

    function M() {
        return months[self.getMonth()];
    }

    function d() {
        var d = self.getDate() + 1;
        return d < 10 ? "0" + d : d;
    }

    function D() {
        return days[self.getDay()];
    }

    function H() {
        return self.getHours() < 10 ? "0" + self.getHours() : self.getHours();
    }

    function i() {
        return self.getMinutes() < 10 ? "0" + self.getMinutes() : self.getMinutes();
    }

    function s() {
        return self.getSeconds() < 10 ? "0" + self.getSeconds() : self.getSeconds();
    }

    var self = this;
    if (time) {
        var prevTime = self.getTime();
        self.setTime(time);
    }

    var spl = input.split("");
    var res = [];
    for (var c = 0; c < spl.length; c++) {
        if (methods.indexOf(spl[c]) >= 0)
            res[c] = eval(spl[c] + "()");
        else
            res[c] = spl[c];
    }

    if (prevTime) {
        self.setTime(prevTime);
    }

    profiler.stop();
    return res.join("");
};

function runDate() {
    profiler.start("runDate");
    var date = new Date("2015-04-10 09:21:54");

    for (var i = 0; i < DATE_ROUNDS; i++) {
        date.formatDate("D d M Y (d-m-Y), H:i:s");
        date.setTime(date.getTime() + DATE_STEP);
    }
    profiler.stop();
}
