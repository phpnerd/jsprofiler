<?php

$con = mysqli_connect("localhost", "ocpplanner_php", "google") or die("Failed to connect to server.");
mysqli_select_db($con, "ocpplanner_php") or die("Failed to select database.");

$Q_GET_USERS = "SELECT U.`id`, `username`, U.`name`, `email`, `groupid`, `admin`, G.`name`, G.`plannable` FROM `users` U, `group` G WHERE U.`groupid` = G.`id`";

$stmt = mysqli_prepare($con, $Q_GET_USERS) or die("Failed to prepare query.");
if(!$stmt->execute())
    die("Failed to execute query. (".$stmt->error.")");
$stmt->bind_result($id, $username, $name, $email, $groupid, $admin, $groupname, $plannable);
$users = array();
while($stmt->fetch())
    $users[count($users)] = array(
            'id' => $id,
            'username' => $username,
            'name' => $name,
            'email' => $email,
            'group' => array('id' => $groupid, 'name' => $groupname, 'plannable' => $plannable == 1),
            'admin' => $admin == 1
        );
echo json_encode($users);

mysqli_close($con);

?>