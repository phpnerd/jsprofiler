var STR_ROUNDS = 3e5;
var STR_LENGTH = 11;
var STR_WORDLEN = 220;

var chars = [ "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", " " ];

function rand(max) {
  return Math.round(Math.random() * max) % max;
}

String.randomC = function(length) {
  profiler.start("String.randomC");
  var s = new String();
  var c;
  for(var i = 0; i < length; i++) {
    c = chars[rand(chars.length)];
    c = i % 2 === 0 ? c.toUpperCase() : c.toLowerCase();
    s += c;
  }
  return profiler.stop("String.randomC", s.trim());
}

function runString() {
  profiler.start("runString");
  var str = new String();
  var words = 0;
  var s;
  for(var i = 0; i < STR_ROUNDS; i++) {
    s = String.randomC(STR_LENGTH);
    str = str.concat(str, s);
    if (str.length > STR_WORDLEN) {
      str.split(" ");
      str.charAt(765);
      str.substring(333, 666);
      str = new String();
      words++;
    }
  }
  if (words < STR_ROUNDS * STR_LENGTH / STR_WORDLEN)
    console.error("Invalid number of words! (" + words + ")");
  profiler.stop("runString");
}
