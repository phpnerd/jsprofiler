package nl.infis.jsprofiler;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.infis.jsprofiler.util.FileIO;

import org.mozilla.javascript.ast.AstRoot;

public class ProfilerHandler
{

	private static final String JS_REGEX = "^(.*).js$",
		OTHER_REGEX = "^(.*).(htm|html|php)$",

	SCRIPT_REGEX = "<script(.*?)type=('|\"|)text/javascript('|\"|)(.*?)>(.*?)</script>";

	private static Pattern SCRIPT_PATTERN = Pattern.compile(SCRIPT_REGEX, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

	private final JSProfilerOptions options;

	private final ProfilerInjector injector;

	/**
	 * Constructor method that sets the options and initializes the ProfilerInjector.
	 *
	 * @param options
	 * 	The JSProfilerOptions to be used while inserting profiler
	 */
	public ProfilerHandler(JSProfilerOptions options)
	{
		this.options = options;
		injector = new ProfilerInjector();
	}

	/**
	 * Method that handles the input file or directory by calling the right methods.
	 * This method also stores the Profiler.js file if -pf parameter is set.
	 *
	 * @throws IOException
	 * 	Is thrown when an error occurred while reading or writing file(s)
	 */
	public void handle() throws IOException
	{
		if (options.inputIsDirectory())
		{
			handleDir();
		}
		else
		{
			handleFile();
		}
		if (options.hasProfilerFile())
		{
			storeProfiler(options.getProfilerFile());
		}
		System.out.println("Done!");
	}

	/**
	 * Method that handles the insertion of the profiler to a single file.
	 *
	 * @throws IOException
	 * 	Is thrown when an error occurred while reading or writing file(s)
	 */
	private void handleFile() throws IOException
	{
		System.out.println(" Input file: " + options.getInputFile().getPath());
		System.out.println("Output file: " + options.getOutputFile().getPath());
		handleFile(options.getInputFile(), options.getOutputFile());
	}

	/**
	 * Method that handles a given file and outputs the result to the given output file.
	 *
	 * @param inFile
	 * 	The file to process
	 * @param outFile
	 * 	The file to write the result to
	 * @throws IOException
	 * 	Is thrown when an error occurred while reading or writing file(s)
	 */
	private void handleFile(File inFile, File outFile) throws IOException
	{
		if (outFile.exists())
		{
			if (!askConfirmation(outFile.getPath() + " already exists. Overwrite file? "))
			{
				System.out.println("Skipped!");
				return;
			}
		}
		if (inFile.getName().matches(OTHER_REGEX))
		{
			handlePartialFile(inFile, outFile);
			return;
		}
		else if (!inFile.getName().matches(JS_REGEX))
		{
			FileIO.copyFile(inFile, outFile);
			return;
		}
		String readableName = createProfilerFunctionName(inFile);
		AstRoot root = JSParser.parse(FileIO.readFile(inFile));
		injector.setFile(root, readableName);
		injector.process();
		FileIO.writeFile(outFile, injector.getRootCode());
	}

	/**
	 * Method that handles a file that contains parts of JavaScript code. Examples are .html files
	 * that contain script-tags, including JavaScript source.
	 *
	 * @param inFile
	 * 	The file to process
	 * @param outFile
	 * 	The file to write the result to
	 * @throws IOException
	 * 	Is thrown when an error occurred while reading or writing file(s)
	 */
	private void handlePartialFile(File inFile, File outFile) throws IOException
	{
		String readableName = createProfilerFunctionName(inFile);
		String source = FileIO.readFile(inFile);
		Matcher scriptMatcher = SCRIPT_PATTERN.matcher(source);
		while (scriptMatcher.find())
		{
			String content = scriptMatcher.group(5);
			if (content.trim().isEmpty())
			{
				continue;
			}
			AstRoot root = JSParser.parse(content);
			injector.setFile(root, readableName);
			injector.process();
			source = source.replace(content, '\n' + injector.getRootCode());
		}
		FileIO.writeFile(outFile, source);
	}

	/**
	 * Method to handle a directory, including all subdirectories and files.
	 *
	 * @throws IOException
	 * 	Is thrown when an error occurred while reading or writing file(s)
	 */
	private void handleDir() throws IOException
	{
		System.out.println(" Input directory: " + options.getInputFile().getAbsolutePath());
		System.out.println("Output directory: " + options.getOutputFile().getAbsolutePath());
		if (!options.getOutputFile().exists())
		{
			options.getOutputFile().mkdirs();
		}
		boolean overwriteDir = options.isInputEqualsOutput() &&
			askConfirmation("Destination directory equals source directory. Do you want to overwrite the source file?");
		if (!overwriteDir && options.getOutputFile().listFiles().length > 0)
		{
			if (askConfirmation("Existing contents of the destination folder will be removed. Do you want to continue?"))
			{
				FileIO.emptyDir(options.getOutputFile());
			}
			else
			{
				System.out.println("Cannot overwrite directory, nothing to do here.");
				System.exit(0);
			}
		}
		if (overwriteDir)
		{
			options.setForced(true);
		}
		handleDir(options.getInputFile());
	}

	/**
	 * Method that processes a given directory and outputs to the directory that is stored in the options.
	 *
	 * @param dirIn
	 * 	The directory to apply the profiler to
	 * @throws IOException
	 * 	Is thrown when an error occurred while reading or writing file(s)
	 */
	private void handleDir(File dirIn) throws IOException
	{
		for (File f : dirIn.listFiles())
		{
			if (f.isDirectory())
			{
				File destDir = constructDestinationFile(f);
				if (!destDir.exists())
				{
					destDir.mkdir();
				}
				handleDir(f);
			}
			else
			{
				handleFile(f, constructDestinationFile(f));
			}
		}
	}

	/**
	 * Method that writes the Profiler.js file in the given file.
	 *
	 * @param resultFile
	 * 	The File to write the source of the profiler itself to
	 * @throws IOException
	 * 	Is thrown when an error occurred while reading or writing file(s)
	 */
	private void storeProfiler(File resultFile) throws IOException
	{
		if (resultFile.exists())
		{
			resultFile.delete();
		}
		else
		{
			resultFile.getParentFile().mkdirs();
		}
		System.out.println("Copying Profiler.js..");
		Files.copy(this.getClass().getResourceAsStream("/Profiler.js"), resultFile.toPath());
	}

	/**
	 * Method that constructs a filepath from an input file, so that its relative path in the output
	 * directory remains the same as it was in the input directory. The File is returned instead of
	 * a path, since the file is requested.
	 *
	 * @param fileIn
	 * 	The original file
	 * @return The file in the output directory corresponding to the same file in the input directory
	 */
	private File constructDestinationFile(File fileIn)
	{
		String path = fileIn.getPath();
		path = path.replaceFirst(options.getInputFile().getPath(), options.getOutputFile().getPath());
		return new File(path);
	}

	/**
	 * Method that prompts the user for confirmation and returns true if the user confirms by pressing
	 * 'Y' or false when pressing 'n' or 'N'.
	 *
	 * @param message
	 * 	The confirmation message to display to the user
	 * @return true if the user agrees, false if not
	 */
	private boolean askConfirmation(String message)
	{
		if (options.isForced())
		{
			return true;
		}
		Scanner in = new Scanner(System.in);
		String line = null;
		while (line == null)
		{
			System.out.print(message + " (Y/n): ");
			line = in.nextLine();
			if (line.equals("Y"))
			{
				return true;
			}
			else if (line.equalsIgnoreCase("N"))
			{
				return false;
			}
			line = null;
		}
		return false;
	}

	/**
	 * Method that extracts the filename from a file by stripping the rest of the path.
	 * If full paths may be used, the full (relative) path is returned.
	 *
	 * @param file
	 * 	The file to extract the filename from
	 * @return The filename to use in the profiler for this particular file
	 */
	private String createProfilerFunctionName(File file)
	{
		if (options.isFullFilepath())
		{
			return file.getPath();
		}
		return file.getName().replaceAll("^(.*)/(.*)", "$2");
	}

}
