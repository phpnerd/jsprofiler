package nl.infis.jsprofiler.old.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomas on 19-5-15.
 */
public class FileTools
{

	public static List<String> readFile(String fileName) {
		File file = new File(fileName);
		return readFile(file);
	}

	public static List<String> readFile(File file) {
		List<String> result = new ArrayList<>();
		if (!file.exists()) {
			System.err.println(file.getAbsolutePath() + " does not exist!");
			return result;
		}
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while((line = br.readLine()) != null)
				result.add(line);
			br.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return result;
	}

	public static void writeFile(String filename, List<String> contents) {
		File file = new File(filename);
		writeFile(file, contents);
	}

	public static void writeFile(File file, List<String> contents) {
		if (file.exists())
			file.delete();
		try {
			PrintWriter pw = new PrintWriter(new FileWriter(file));
			contents.forEach(pw::println);
			pw.flush();
			pw.close();
		} catch (IOException ex) {
			System.err.println("Filename: " + file.getAbsolutePath());
			ex.printStackTrace();
		}
	}

}
