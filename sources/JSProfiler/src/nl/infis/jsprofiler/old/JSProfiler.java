package nl.infis.jsprofiler.old;

import java.util.List;

import nl.infis.jsprofiler.old.util.FileTools;

import org.apache.commons.cli.*;

/**
 * Created by thomas on 19-5-15.
 */

public class JSProfiler
{

	private static final String ARG_INPUT_FILE 	= "if",
								ARG_OUTPUT_FILE	= "of";

	private static final Options cliOptions = new Options();
	static {
		cliOptions.addOption(new Option(ARG_INPUT_FILE, true, "the input file to apply the profiler to"));
		cliOptions.addOption(new Option(ARG_OUTPUT_FILE, true, "the output file to write to"));
	}

	public static void main(String[] args) {
		CommandLineParser cliParser = new DefaultParser();
		CommandLine cliLine = null;
		try {
			cliLine =  cliParser.parse(cliOptions, args);
		} catch (ParseException ex) {
			ex.printStackTrace();
		}
		if (cliLine == null)
			return;
		if (!cliLine.hasOption(ARG_INPUT_FILE)) {
			System.err.println("Input file is required!");
		} else {
			String inputFilename = cliLine.getOptionValue(ARG_INPUT_FILE);
			String outputFilename = cliLine.getOptionValue(ARG_OUTPUT_FILE);
			System.out.println("Input file: " + inputFilename);
			List<String> content = FileTools.readFile(inputFilename);
			ProfilerInjecter pi = new ProfilerInjecter(content);
			pi.process();
			FileTools.writeFile(outputFilename, pi.getFileContents());
		}
	}

}
