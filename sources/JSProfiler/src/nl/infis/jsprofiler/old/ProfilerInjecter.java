package nl.infis.jsprofiler.old;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by thomas on 20-5-15.
 */
public class ProfilerInjecter {

    private static final String REGEX_BASIC_FUNCTION    = "function( +)(\\b+)( *)\\((.*)\\)( *)\\{((.|\\s)*?)\\}";
    private static final String REPLACE_BASIC_FUNCTION  = "function$1$2$3($4)$5{\nprofiler.start('$2');$6profiler.stop('$2');\n}";

    private static String   fileContents,
                            newFileContents;

    public ProfilerInjecter(List<String> fileContents) {
        StringBuilder sb = new StringBuilder();
        fileContents.forEach(s -> sb.append(s + '\n'));
        this.fileContents = sb.toString();
        newFileContents = "";
    }

    public void process() {
        newFileContents = fileContents.replaceAll(REGEX_BASIC_FUNCTION, REPLACE_BASIC_FUNCTION);
    }

    public List<String> getFileContents() {
        return new ArrayList<String>(Arrays.asList(newFileContents.split("\\n")));
    }

}
