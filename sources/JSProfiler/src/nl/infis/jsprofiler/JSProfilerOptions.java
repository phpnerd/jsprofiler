package nl.infis.jsprofiler;

import java.io.File;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class JSProfilerOptions
{

	private static final String ARG_INPUT_FILE = "if",
		ARG_OUTPUT_FILE = "of",
		ARG_PROFILER_FILE = "pf",
		ARG_FULL_FILEPATH = "fullpath",
		ARG_FORCE = "f",
		ARG_HELP = "help";

	private static final Options cliOptions = new Options();

	static
	{
		cliOptions.addOption(new Option(ARG_INPUT_FILE, true, "the input file or directory to apply the profiler to"));
		cliOptions.addOption(new Option(ARG_OUTPUT_FILE, true, "the output file or directory to write to"));
		cliOptions.addOption(new Option(ARG_PROFILER_FILE, true, "the path in which the Profiler.js should be placed"));
		cliOptions.addOption(new Option(ARG_FULL_FILEPATH, false, "use full file paths instead of filenames in profiler"));
		cliOptions.addOption(new Option(ARG_FORCE, false, "force required file removals and overrides"));
		cliOptions.addOption(new Option(ARG_HELP, false, "displays help message"));
	}

	private final File inputFile, outputFile, profilerFile;

	private boolean forced, fullFilepath;

	/**
	 * JSProfilerOptions constructor that reads the CLI arguments and sets the options for this run of the JSProfiler.
	 *
	 * @param args
	 * 	String arguments that are passed through the CLI and the main()
	 * @throws IllegalArgumentException
	 * 	Is thrown when no valid arguments could be found
	 */
	public JSProfilerOptions(String... args) throws IllegalArgumentException
	{
		CommandLine cliLine = parseArgs(args);
		if (cliLine.hasOption(ARG_HELP))
		{
			displayHelp();
		}
		if (!cliLine.hasOption(ARG_INPUT_FILE))
		{
			throw new IllegalArgumentException("No input file specified!");
		}
		inputFile = new File(cliLine.getOptionValue(ARG_INPUT_FILE));
		outputFile = new File(cliLine.getOptionValue(cliLine.hasOption(ARG_OUTPUT_FILE) ? ARG_OUTPUT_FILE : ARG_INPUT_FILE));
		profilerFile = createProfilerFile(cliLine);
		forced = cliLine.hasOption(ARG_FORCE);
		fullFilepath = cliLine.hasOption(ARG_FULL_FILEPATH);
	}

	/**
	 * Static method that parses the CLI and catches ParseExceptions.
	 *
	 * @param args
	 * 	The CLI arguments from the main()
	 * @return The CommandLine object to extract arguments from
	 * @throws IllegalArgumentException
	 * 	Is thrown when no valid arguments could be found
	 */
	private static CommandLine parseArgs(String... args) throws IllegalArgumentException
	{
		CommandLineParser cliParser = new DefaultParser();
		CommandLine cliLine = null;
		try
		{
			cliLine = cliParser.parse(cliOptions, args);
		}
		catch (ParseException ex)
		{
			displayHelp();
		}
		if (cliLine == null)
		{
			throw new IllegalArgumentException("No valid arguments could be found!");
		}
		return cliLine;
	}

	/**
	 * Static method that creates a file object from the CommandLine object for the Profiler file.
	 * If a *.js file is passed, this file is returned, otherwise, a directory is prepended.
	 *
	 * @param cliLine
	 * 	The CommandLine object to parse the filename from
	 * @return The File object to write the Profiler.js file to
	 */
	private static File createProfilerFile(CommandLine cliLine)
	{
		if (!cliLine.hasOption(ARG_PROFILER_FILE))
		{
			return null;
		}
		String filename = cliLine.getOptionValue(ARG_PROFILER_FILE);
		boolean isDir = !filename.matches("^(.*).js$");
		if (isDir)
		{
			filename = filename.replace("/$", "") + "/Profiler.js";
		}
		return new File(filename);
	}

	public boolean inputIsDirectory()
	{
		return inputFile.isDirectory();
	}

	public boolean hasProfilerFile()
	{
		return profilerFile != null;
	}

	public boolean isInputEqualsOutput()
	{
		return inputFile.getAbsolutePath().equals(outputFile.getAbsolutePath());
	}

	public File getInputFile()
	{
		return inputFile;
	}

	public File getOutputFile()
	{
		return outputFile;
	}

	public File getProfilerFile()
	{
		return profilerFile;
	}

	public boolean isForced()
	{
		return forced;
	}

	public void setForced(boolean forced)
	{
		this.forced = forced;
	}

	public boolean isFullFilepath()
	{
		return fullFilepath;
	}

	/**
	 * Displays a help text that explains the possible arguments for this application.
	 */
	private static void displayHelp()
	{
		HelpFormatter helpFormatter = new HelpFormatter();
		helpFormatter.printHelp("JSProfiler", cliOptions);
	}

}
