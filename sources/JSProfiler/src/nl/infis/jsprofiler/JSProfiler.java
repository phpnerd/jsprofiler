package nl.infis.jsprofiler;

import java.io.IOException;

public class JSProfiler
{

	/**
	 * Main method of the JSProfiler. This method creates a JSProfilerOptions object
	 * and a ProfilerHandler, which then handles the addition of the profiler to the
	 * existing project or file.
	 *
	 * @param args
	 * 	The default argumnets that can be passed
	 */
	public static void main(String[] args)
	{
		try
		{
			JSProfilerOptions options = new JSProfilerOptions(args);
			ProfilerHandler handler = new ProfilerHandler(options);
			handler.handle();
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
		catch (IllegalArgumentException ex)
		{
			System.err.println(ex.getMessage());
		}
	}

}
