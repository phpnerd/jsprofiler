package nl.infis.jsprofiler.util;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class FileIO
{

	/**
	 * Static method that reads a given file to a String.
	 *
	 * @param file
	 * 	The file to read
	 * @return A String containing the contents of the file
	 */
	public static String readFile(File file)
	{
		StringBuilder sb = new StringBuilder();
		if (!file.exists())
			return sb.toString();
		try
		{
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null)
				sb.append(line).append('\n');
			br.close();
		}
		catch (FileNotFoundException ex)
		{
			System.err.println("Filename: " + file.getAbsolutePath());
			ex.printStackTrace();
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
		return sb.toString();
	}

	/**
	 * Static method that writes the given contents to a given file.
	 *
	 * @param file
	 * 	The file to write to
	 * @param contents
	 * 	The contents, as String, to write to the file
	 */
	public static void writeFile(File file, String contents)
	{
		if (file.exists())
			file.delete();
		try
		{
			PrintWriter pw = new PrintWriter(new FileWriter(file));
			pw.write(contents);
			pw.flush();
			pw.close();
		}
		catch (IOException ex)
		{
			System.err.println("Filename: " + file.getAbsolutePath());
			ex.printStackTrace();
		}
	}

	/**
	 * Static method to copy one file to another.
	 *
	 * @param src
	 * 	The source file to copy
	 * @param dest
	 * 	The destination file to copy to
	 * @throws FileNotFoundException
	 * 	Is thrown when the source file cannot be found
	 * @throws IOException
	 * 	Is thrown when reading the file or writing the file fails
	 */
	public static void copyFile(File src, File dest) throws FileNotFoundException, IOException
	{
		if (!src.exists())
		{
			throw new FileNotFoundException(src.getPath() + " doesn't exist!");
		}
		Files.copy(src.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
	}

	/**
	 * Static recursive method to remove all contents of a given directory.
	 *
	 * @param dir
	 * 	The directory to empty.
	 * @throws IOException
	 * 	Is thrown when the given file isn't a directory
	 */
	public static void emptyDir(File dir) throws IOException
	{
		if (!dir.isDirectory())
		{
			throw new IOException(dir.getPath() + " is not a directory!");
		}
		for (File child : dir.listFiles())
		{
			if (child.isDirectory())
			{
				emptyDir(child);
			}
			child.delete();
		}
	}

}
