package nl.infis.jsprofiler;

import org.mozilla.javascript.CompilerEnvirons;
import org.mozilla.javascript.IRFactory;
import org.mozilla.javascript.ast.AstRoot;

import java.io.IOException;
import java.io.StringReader;

public class JSParser
{

	private static final CompilerEnvirons env = new CompilerEnvirons();

	static
	{
		env.setRecoverFromErrors(true);
		env.setGenerateDebugInfo(true);
		env.setRecordingComments(true);
	}

	/**
	 * Method to parse the given JavaScript source to an AstRoot, using the Rhino parser.
	 *
	 * @param source
	 * 	The source to parse
	 * @return The AST (as AstRoot) of the given source
	 */
	public static AstRoot parse(String source)
	{
		StringReader stringReader = new StringReader(source);
		IRFactory factory = new IRFactory(env);
		AstRoot node = new AstRoot();
		try
		{
			node = factory.parse(stringReader, null, 1);
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
		return node;
	}

}
