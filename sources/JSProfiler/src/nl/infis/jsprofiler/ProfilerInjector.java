package nl.infis.jsprofiler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.mozilla.javascript.Token;
import org.mozilla.javascript.ast.*;

public class ProfilerInjector
{

	private static final String ANONYMOUS_FUNCTION_PREFIX = "anonymousFunction_",
		PROFILER_OBJECT = "profiler",
		PROFILER_START_METHOD = "start",
		PROFILER_STOP_METHOD = "stop";

	private static final char STRING_QUOTE = '\'';

	private AstRoot root;

	private String filename;

	private int anonymousCounter;

	/**
	 * Constructor that initializes the counter for anonymous functions.
	 */
	public ProfilerInjector()
	{
		anonymousCounter = 1;
	}

	/**
	 * Method that sets AstRoot root and name of the file the Injector is going to process.
	 * This method is somewhat of an extension of the constructor, but can be used to load
	 * other scripts within the same run, without resetting the anonymous function counter
	 * or initializing a new Injector.
	 *
	 * @param root
	 * 	The AstRoot of the parsed script which shall be processed
	 * @param filename
	 * 	The name of the file that is being processed
	 */
	public void setFile(AstRoot root, String filename)
	{
		this.root = root;
		this.filename = filename;
	}

	/**
	 * Method that starts processing the given AstRoot, adding profiler statements to the code.
	 */
	public void process()
	{
		System.out.println("Processing " + filename + "..");
		findAndProcessFunctionNodes(root);
	}

	/**
	 * Method that uses a NodeVisitor to find all function declarations within the root and
	 * processes every function found. The subtree of each function node won't be included
	 * in this search, since this subtree is covered recursively after adding a start
	 * statement to the node in processFunction().
	 *
	 * @param node
	 * 	The root node to start visiting
	 */
	private void findAndProcessFunctionNodes(AstNode node)
	{
		node.visit((AstNode n) -> {
			if (functionNodeFilter(n))
			{
				processFunction((FunctionNode) n);
				return false;
			}
			return true;
		});
	}

	/**
	 * Method that processes a single function node. Adds a start-statement in front of the
	 * existing function body, processes its contents recursively (in search for other
	 * function declarations) and adds stop-statements to returns and, if required, to
	 * the end of the function body. The function body must be processed recursively
	 * before adding stop-statements to return-statements, otherwise all return statements
	 * in child functions will get the name of their root function.
	 *
	 * @param functionNode
	 * 	The function node to process
	 */
	private void processFunction(FunctionNode functionNode)
	{
		final String functionName = getFunctionName(functionNode);
		AstNode fBody = functionNode.getBody();
		int lineNo = functionNode.getLineno();
		fBody.addChildToFront(createProfilerNode(PROFILER_START_METHOD, lineNo, functionName));
		findAndProcessFunctionNodes(fBody);
		addProfilerStop(functionName, fBody, lineNo);
	}

	/**
	 * Method that returns the source of the root as String.
	 *
	 * @return The source code of the root
	 */
	public String getRootCode()
	{
		return root.toSource(0);
	}

	/**
	 * Method to extract the name of a given function node. If the function has no name,
	 * the function will be identified with a unique number, prepended with the
	 * ANONYMOUS_FUNCTION_PREFIX.
	 *
	 * @param functionNode
	 * 	The function node to extract the name from
	 * @return The function name of the given function node
	 */
	private String getFunctionName(FunctionNode functionNode)
	{
		if (!(functionNode.getName() == null || functionNode.getName().isEmpty()))
		{
			return functionNode.getName();
		}
		AstNode parent = functionNode.getParent();
		switch (parent.getType())
		{
		case Token.VAR:
			VariableInitializer vi = (VariableInitializer) parent;
			Name functionNameNode = (Name) vi.getTarget();
			return functionNameNode.getIdentifier();
		case Token.ASSIGN:
			Assignment assignNode = (Assignment) parent;
			return propToString(assignNode.getLeft());
		case Token.COLON:
			return propToString(parent);
		default:
			return ANONYMOUS_FUNCTION_PREFIX + (anonymousCounter++);
		}
	}

	/**
	 * Method that takes care of the addition of the stop-statement(s) to a given function body.
	 *
	 * @param functionName
	 * 	The name of the function that is being processed
	 * @param functionBody
	 * 	The function body to add stop-statements to
	 * @param lineNo
	 * 	The line number on which this function is declared
	 */
	private void addProfilerStop(String functionName, AstNode functionBody, int lineNo)
	{
		List<AstNode> returnStatements = getReturnStatements(functionBody);
		if (!returnStatements.isEmpty())
		{
			returnStatements.forEach(rs -> createProfilerAdvancedStopNode(functionName, rs, lineNo));
		}
		if (functionBody.getLastChild().getType() != Token.RETURN)
		{
			functionBody.addChildToBack(createProfilerNode(PROFILER_STOP_METHOD, lineNo, functionName));
		}
	}

	/**
	 * Method that constructs an advanced stop-statement, yielding a non-empty return.
	 * The original return node is then replaced with the new one.
	 *
	 * @param functionName
	 * 	The name of the function
	 * @param returnNode
	 * 	The node containing the return value for this function
	 * @param lineNo
	 * 	The line number on which this function is declared
	 */
	private void createProfilerAdvancedStopNode(String functionName, AstNode returnNode, int lineNo)
	{
		ReturnStatement returnStatement = (ReturnStatement) returnNode;
		ReturnStatement newReturnStatement =
			createProfilerNode(PROFILER_STOP_METHOD, lineNo, functionName, returnStatement.getReturnValue());
		replaceChildNode(returnStatement, newReturnStatement);
	}

	/**
	 * Method that constructs a node that describes a function call to the profiler.
	 * Since the method is passed, this is dynamic. The resulting source code could
	 * be something like the following:
	 * <i>profiler.[method]('[filename]', [lineNo], '[functionName]');</i>
	 *
	 * @param method
	 * 	The method of the profiler object that should be called
	 * @param lineNo
	 * 	The line number on which the function is declared
	 * @param functionName
	 * 	The name of the function
	 * @return The ExpressionStatement that describes the function call to the profiler
	 */
	private ExpressionStatement createProfilerNode(String method, int lineNo, String functionName)
	{
		Name profilerNameNode = new Name();
		Name methodNameNode = new Name();
		profilerNameNode.setIdentifier(PROFILER_OBJECT);
		methodNameNode.setIdentifier(method);

		PropertyGet propertyNode = new PropertyGet();
		propertyNode.setTarget(profilerNameNode);
		propertyNode.setProperty(methodNameNode);

		StringLiteral functionNameNode = new StringLiteral();
		functionNameNode.setValue(functionName);
		functionNameNode.setQuoteCharacter(STRING_QUOTE);

		FunctionCall profilerStatement = new FunctionCall();
		profilerStatement.setTarget(propertyNode);
		if (method.equals(PROFILER_START_METHOD))
		{
			StringLiteral filenameNode = new StringLiteral();
			filenameNode.setValue(filename);
			filenameNode.setQuoteCharacter(STRING_QUOTE);
			NumberLiteral lineNoNode = new NumberLiteral();
			lineNoNode.setValue(Integer.toString(lineNo));

			profilerStatement.addArgument(filenameNode);
			profilerStatement.addArgument(lineNoNode);
		}
		profilerStatement.addArgument(functionNameNode);

		ExpressionStatement expression = new ExpressionStatement();
		expression.setExpression(profilerStatement);

		return expression;
	}

	/**
	 * Method that constructs a node that describes a function call to the profiler.
	 * As before, the method that is called is dynamic. This call, however, includes
	 * a return value, thus yields a ReturnStatement.
	 *
	 * @param method
	 * 	The method of the profiler object that should be called
	 * @param lineNo
	 * 	The line number on which the function is declared
	 * @param functionName
	 * 	The name of the function
	 * @param returnValue
	 * 	The value (as AstNode) to be returned by this return
	 * @return The ReturnStatement that describes a return statement with a call to the
	 * profiler object.
	 */
	private ReturnStatement createProfilerNode(String method, int lineNo, String functionName, AstNode returnValue)
	{
		ReturnStatement returnStatement = new ReturnStatement();
		ExpressionStatement expression = createProfilerNode(method, lineNo, functionName);
		FunctionCall functionCall = (FunctionCall) expression.getExpression();
		if (returnValue == null)
		{
			KeywordLiteral nullNode = new KeywordLiteral();
			nullNode.setType(Token.NULL);
			functionCall.addArgument(nullNode);
		}
		else
		{
			functionCall.addArgument(returnValue);
		}
		returnStatement.setReturnValue(functionCall);
		return returnStatement;
	}

	/**
	 * Method that collects all return statements from the function body. Child functions
	 * will be ignored, since those return statements are in a different scope.
	 *
	 * @param functionBody
	 * 	The function body to search
	 * @return A List of return statements (as AstNode)
	 */
	private static List<AstNode> getReturnStatements(AstNode functionBody)
	{
		List<AstNode> returnNodes = new ArrayList<>();
		functionBody.visit(node -> {
			if (node.getType() == Token.FUNCTION)
			{
				return false;
			}
			if (node.getType() == Token.RETURN)
			{
				returnNodes.add(node);
			}
			return true;
		});
		return returnNodes;
	}

	/**
	 * Method that constructs a pretty name from an AstNode that is known to contain the
	 * name of a function. Some examples are:
	 * { x: 5, callback: function() { ... } } 			-> "callback"
	 * Date.prototype.formatDate = function() { ... } 	-> "Date.prototype.formatDate"
	 *
	 * @param node
	 * 	The node to extract the name from
	 * @return A pretty name extracted from the given node
	 */
	private static String propToString(AstNode node)
	{
		switch (node.getType())
		{
		case Token.NAME:
			return ((Name) node).getIdentifier();
		case Token.GETPROP:
			PropertyGet pNode = (PropertyGet) node;
			return propToString(pNode.getTarget()) + "." + propToString(pNode.getProperty());
		case Token.COLON:
			return propToString(((ObjectProperty) node).getLeft());
		default:
			return "";
		}
	}

	/**
	 * Wrapper method to replace a given node in the AST with another one. Since Rhino
	 * results in a NullPointerException if a node that is to be replaced is the only
	 * child, this method catches the exception and then performs an alternative
	 * replace.
	 *
	 * @param node
	 * 	The node to be replaced
	 * @param replace
	 * 	The replacement for the node
	 */
	private void replaceChildNode(AstNode node, AstNode replace)
	{
		AstNode parent = node.getParent();
		try
		{
			parent.replaceChild(node, replace);
		}
		catch (NullPointerException ex)
		{
			replaceNoScopeNode(node, replace);
		}
	}

	/**
	 * Method that replaces a given node with another one. This is, however, a hard-replace,
	 * setting the full body of the scope this source node is contained in to a the given
	 * replacement node. This method is only called upon
	 *
	 * @param child
	 * 	The node to replace
	 * @param replace
	 * 	The replacement node
	 */
	private void replaceNoScopeNode(AstNode child, AstNode replace)
	{
		AstNode parent = child.getParent();
		switch (parent.getType())
		{
		case Token.IF:
			IfStatement ifStmt = (IfStatement) parent;
			if (ifStmt.getThenPart() == child)
			{
				ifStmt.setThenPart(replace);
			}
			else if (ifStmt.getElsePart() == child)
			{
				ifStmt.setElsePart(replace);
			}
			return;
		case Token.CASE:
			SwitchCase swCase = (SwitchCase) parent;
			swCase.setStatements(Collections.singletonList(replace));
			return;
		case Token.FOR:
			ForLoop forStmt = (ForLoop) parent;
			forStmt.setBody(replace);
			return;
		case Token.WHILE:
			WhileLoop whileStmt = (WhileLoop) parent;
			whileStmt.setBody(replace);
			return;
		case Token.DO:
			DoLoop doStmt = (DoLoop) parent;
			doStmt.setBody(replace);
			return;
		default:
			System.err.println("Failed to replace node: " + filename + ":" + child.getLineno());
		}
	}

	/**
	 * Filter method to be used with a NodeVisitor.
	 *
	 * @param node
	 * 	The node to check
	 * @return true if the given node is a FunctionNode, false if not
	 */
	private static boolean functionNodeFilter(AstNode node)
	{
		return node.getType() == Token.FUNCTION;
	}

}
