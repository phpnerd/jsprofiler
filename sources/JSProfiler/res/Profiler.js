/**
 * FunctionCall constructor which creates a new FunctionCall object with the given name.
 * A FunctionCall holds information about a function call or a set of calls to the same
 * function.
 * @param functionName The name of the function called
 * @param filename The name of the file this function is declared in
 * @param lineNo The line number in which this function is declared
 * @constructor
 */
var FunctionCall = function (functionName, filename, lineNo) {
  this.functionName = functionName;
  this.file = filename;
  this.line = lineNo;
  this.totalTime = 0;
  this.startTime = new Date();
  this.calls = 1;
  this.averageTime = 0;
  this.minTime = Number.MAX_VALUE;
  this.maxTime = -Number.MAX_VALUE;
  this.subFunctions = [];
};

/**
 * FunctionCall.calculateTime ends the profiling of a specific function and calculates
 * the time the function took to execute. If the FunctionCall consisted of more than one
 * call to the function, the total execution time as well as the average, min and max
 * are updated.
 */
FunctionCall.prototype.calculateTime = function () {
  var endTime = new Date() - this.startTime;
  if (this.calls > 1) {
    this.averageTime = ((this.calls - 1) * this.averageTime + endTime) / this.calls;
  } else {
    this.averageTime = endTime;
  }
  if (endTime < this.minTime) {
    this.minTime = endTime;
  }
  if (endTime > this.maxTime) {
    this.maxTime = endTime;
  }
  this.totalTime += endTime;
};

/**
 * addCall increases the number of calls for this function by one and sets the startTime
 * to eventually calculate the time from.
 */
FunctionCall.prototype.addCall = function () {
  this.calls++;
  this.startTime = new Date();
};

RootCall.prototype = new FunctionCall();
RootCall.prototype.constructor = RootCall;
/**
 * RootCall extends FunctionCall and should be used as root of the profile-tree. It has
 * a fixed name ("Main") and doesn't contains attributes supporting multiple function
 * calls, since there is no need for multiple calls to Main.
 * @constructor
 */
function RootCall() {
  this.functionName = 'Main';
  this.totalTime = 0;
  this.idleTime = null;
  this.startTime = new Date();
  this.subFunctions = [];
}

/**
 * RootCall.calculateTime calculates the final executionTime and idleTime for Main.
 */
RootCall.prototype.calculateTime = function () {
  this.totalTime = new Date() - this.startTime;
  var idleTime = this.totalTime;
  this.subFunctions.forEach(function (e) {
    idleTime -= e.totalTime;
  });
  this.idleTime = idleTime;
};

/**
 * The Profiler is used to create a profile of the web application and is called at
 * every function call. The Profiler contains a profile tree and keeps track of the
 * "location" in the tree in which the application is now executing.
 * @constructor
 */
var Profiler = function () {

  /* === ATTRIBUTE DECLARATIONS === */
  var active = true,
    profile = new RootCall('Main'),
    depth = 0;

  /* === PUBLIC METHODS === */

  /**
   * Start is called when a JavaScript function starts executing and creates a new
   * FunctionCall object in the profile or adds a call to the existing FunctionCall.
   * @param filename The filename of the file in which the function is declared
   * @param lineNo The line number on which the function is declared
   * @param functionName The name of the function being executed
   */
  this.start = function (filename, lineNo, functionName) {
    if (!assertActive()) {
      return;
    }
    var currentScope = getScope(),
      appendCall = findElement(currentScope, functionName);
    if (appendCall !== null) {
      appendCall.addCall();
    } else {
      currentScope.push(new FunctionCall(functionName, filename, lineNo));
    }
    depth++;
  };

  /**
   * Stop is called when a function finished executing. The FunctionCall is closed by
   * calculating the execution time. If a return value should be passed on, this value
   * may be given as argument.
   * @param functionName The name of the function to stop profiling
   * @param returnValue The return value for the function being profiled
   * @returns {*} The return value received as argument
   */
  this.stop = function (functionName, returnValue) {
    if (!assertActive()) {
      return returnValue;
    }
    depth--;
    var currentScope = getScope(),
      appendCall = findElement(currentScope, functionName);
    if (appendCall !== null) {
      currentScope = appendCall;
    } else {
      currentScope = currentScope[currentScope.length - 1];
    }
    currentScope.calculateTime();
    return returnValue;
  };

  /**
   * Close closes the profiler, thus now allowing any more functions to be profiled.
   * The profiler can only be closed when no functions are currently executing and the
   * profiler is still active. Once the profiler has been closed, the profiler cannot
   * become active again.
   * @returns {*} The resulting profile object is returned
   */
  this.close = function () {
    if (!assertActive()) {
      return;
    } if (depth > 0) {
      console.log('Cannot close profiler. Some methods are still running.');
      return;
    }
    depth = 0;
    var tmp = getRootScope();
    tmp.calculateTime();
    active = false;
    return tmp;
  };

  /**
   * toString method returns the profile as JSON string.
   * @returns {*} The profile as JSON string
   */
  this.toString = function () {
    return JSON.stringify(getRootScope());
  };

  /**
   * Method prints the profile in the console.
   */
  this.print = function () {
    console.log('Profile:\n' + this);
  };

  /* === PRIVATE METHODS === */

  /**
   * Implementation of an assert-like statement. Method checks whether the profiler is
   * active or not.
   * @returns {boolean} true if profiler is active, false if not
   */
  function assertActive() {
    return active;
  }

  /**
   * Method which browses to the current scope in the profile-tree and returns the array
   * that is the current scope, according to the depth which is maintained in the
   * Profiler.
   * @returns {Array} The current scope for the Profiler
   */
  function getScope() {
    var s = profile,
      i = 0;
    for (; i < depth; i++) {
      s = s.subFunctions[s.subFunctions.length - 1];
    }
    return s.subFunctions;
  }

  /**
   * Returns the profile-object, which is the root scope for the profiler.
   * @returns {RootCall} The profile-object
   */
  function getRootScope() {
    return profile;
  }

  /**
   * Finds a FunctionCall element in the given scope with the functionName as specified
   * and returns this element. This method is used to group equal FunctionCalls within
   * the same scope.
   * @param currentScope The scope to search in
   * @param functionName The name of the function to find
   * @returns {*} The element if it exists or null if no FunctionCall can be found
   */
  function findElement(currentScope, functionName) {
    if (currentScope.length === 0) {
      return null;
    }
    if (currentScope[currentScope.length - 1].functionName === functionName) {
      return currentScope[currentScope.length - 1];
    }
    for (var i = 0; i < currentScope.length; i++) {
      if (currentScope[i].functionName === functionName) {
        return currentScope[i];
      }
    }
    return null;
  }

},

// Initialise a new Profiler for use in the web application
profiler = new Profiler();
