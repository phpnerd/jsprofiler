# JSProfiler #

## Short description ##
This repository contains my Master's Thesis. The goal for this thesis was to construct a JavaScript profiler in JavaScript. To integrate this profiler with existing sources, an integration tool has been created as well. A more detailed description can be found in my thesis report.

## Structure ##
All sources can be found in the sources directory. My thesis report and presentation are located in the docs directory. Below is a short description of every directory within sources.

**AccuracyEvaluation**: This is the accuracy evaluation method that was built to measure the accuracy of a method.

**AngularJS Tests**: Contains small example programs built with AngularJS. I've never done anything with AngularJS, so don't expect fancy web applications. They were meant only for my own education.

**JSProfiler**: Contains the sources for the integration tool and the final Profiler.js. A prebuilt version is available as JAR file, but this can also be built from the sources and libraries provided.

**ManualLogging**: The first version of the profiler, including some tests.

**ManualLogging - improved**: The improved version of the profiler, including some tests.

## Tool usage ##
The JSProfiler integration tool in the JSProfiler directory can be used to integrate the JavaScript profiler in an existing application. This tool can be called from the command line and requires some parameters to be set. The following parameters can be used.

**-f** : Force. When set, the user will not be prompted when files are being removed or overwritten.

**-fullpath** : When set, the function name in the call to the profiler is set to the relative path instead of only the name of the source file.

**-help** : When set, a help message is displayed to the user.

**-if** *arg* : Input file. Provides an input file or directory to apply the profiler to. If a directory is passed, all JavaScript files within this directory will be subject to application of the profiler. Other files will be copied.

**-of** *arg* : Output file. Provides an output file or directory to write the output to. If this argument is not set, the tool will overwrite the input file with its output.

**-pf** *arg* : Profiler file. When set, the source of the profiler itself is written to this file. If a directory is given, Profiler.js is written in this directory; if a file (.js) is given, the source is written to this file.

Example usage of the tool can be as follows.

*JSProfiler -if app/ -of app_profiler/ -pf app_profiler/misc/profiler/ -f*

or

*JSProfiler -if app/app.js -f*

## Disclaimer ##
These sources are provided as is, without any kind of warranty. Feel free to contact me if you have any questions. My email is mentioned on the title page of the report.